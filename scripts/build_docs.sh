#!/bin/bash

DOCSTYPE=$1
OLD_PATH=$PATH
PATH=/usr/local/bin:$PATH

if [[ "$DOCSTYPE" == "api" ]]; then
    DOCSTITLE="API"
    DOCSDIR="api"
    TARBALL="docs"
else
    if [[ "$DOCSTYPE" == "manual" ]]; then
        DOCSTITLE="Manual"
        DOCSDIR="manual"
        TARBALL="manual"
    else
        DOCSTITLE="User Guide"
        DOCSDIR="user-guide"
        TARBALL="user-guide"
    fi
fi

echo "Building $DOCSTITLE documentation..."
cd /home/openlp/Projects/documentation/trunk
bzr up
cd $DOCSDIR
echo $DOCSDIR
pwd
rm -fR build
make html
echo "Creating tarball..."
cd build
cp -R html $TARBALL
tar -czvf ${TARBALL}.tar.gz $TARBALL
echo "Uploading tarball..."
scp ${TARBALL}.tar.gz openlp@openlp.org:public_html/
ssh openlp@openlp.org "cd ~/public_html && tar -xzvf ${TARBALL}.tar.gz"
echo "Finished $DOCSTITLE run."
PATH=$OLD_PATH
