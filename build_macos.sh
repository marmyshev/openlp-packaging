#!/usr/bin/env bash

python ./builders/macosx-builder.py --verbose --config ./osx/config.ini.default -b ./../trunk/ --skip-update --skip-translations -d ./../trunk/documentation/ --debug
cp /usr/local/bin/mediainfo ./../trunk/dist/OpenLP.app/Contents/MacOS/mediainfo