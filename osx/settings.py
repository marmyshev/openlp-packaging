import os

# This is the settings file for building the DMG. Run dmgbuild like so:
#  $ dmgbuild -s dmg-settings.py -D size=<size>,app=<path/to/OpenLP.app> "OpenLP" OpenLP-{version}.dmg

HERE = os.getcwd()

format = 'UDBZ'
size = '600M'
files = [defines.get('app', '/Applications/OpenLP.app')]
symlinks = { 'Applications': '/Applications' }
badge_icon = os.path.join(HERE, 'OpenLP.icns')
icon_locations = {
    'OpenLP.app': (130, 110),
    'Applications': (400, 110)
}
background = os.path.join(HERE, 'OpenLP-Background.tiff')
window_rect = ((100, 100), (530, 360))
default_view = 'icon-view'
show_icon_preview = False
arrange_by = None
grid_offset = (0, 0)
label_pos = 'bottom' # or 'right'
text_size = 16
icon_size = 128
